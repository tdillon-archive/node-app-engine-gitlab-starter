const express = require('express')
const app = express()
const port = process.env.PORT || 8080;

app.get('/', (req, res) => res.send(`Simple Express Node app running on Google App Engine.`));

app.listen(port, () => console.log(`Node Express app listening on port ${port}!`))
